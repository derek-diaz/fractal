﻿#pragma strict

    var goFollow:GameObject;
    var speed:float = 0.5f;
 
    function Start () {
       goFollow = Camera.main.gameObject;
    }
 
    function Update () {
       transform.position = goFollow.transform.position;
       transform.rotation  = goFollow.transform.rotation;
    }