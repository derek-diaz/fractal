﻿private var blink = false;
private var counter:int = 0;
private var blinkSpeed:int = 5;
public var tv:GameObject;

function Start(){
}
 
function Update()
{
    if(counter == blinkSpeed)
    {
        blink = true;
        counter = 0;
    } 
    else
        blink = false;
 
    counter++;
    
}
 
function OnGUI()
{
     if(blink)
        tv.active = true;
     else 
        tv.active = false;
}