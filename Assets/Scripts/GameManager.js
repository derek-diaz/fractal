﻿#pragma strict

var player : GameObject;

function Start () {

	//Hide OS Cursor
	Screen.showCursor = false;

}

function Update () {

	if (Input.GetKeyDown("escape")){
		if (Screen.showCursor == false){
			player.SendMessage("ToggleInput", true); //execute method inside FPS Controller 
			Screen.showCursor = true;
			Screen.lockCursor = false;
		}else{
			Screen.lockCursor = true;
			Screen.showCursor = false;
			player.SendMessage("ToggleInput", false);
		}
	}
	

}