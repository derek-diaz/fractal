---------------------------------------------------------
	Model data: Slenderman v1.1
---------------------------------------------------------


Thank you for downloading this model!
I hope you like it! c:


---------------------------------------------------------


****** Rules ******

Editing: allowed
Distribution: allowed only with my permission (please write an e-mail to me or send a note to my deviantart account)


---------------------------------------------------------


****** Notes ******

If you have MikuMikuEffect, I suggest using SS.fx included with the model as it looks nicer.
�This model looks better with edge line off.
If you notice any bugs, please e-mail me and I may repair them.


---------------------------------------------------------


2012/08/23 skye




contact: 
aoikedamono@gmail.com
deadlyneurot0xin.deviantart.com

modelling: TomitakeP, skye


---------------------------------------------------------


****** Update History ******

2012/08/23 Release of version 1.1 (game-like and normal models + red tie, fixed head, sleeve bones, tentacles 8D)
2012/07/08 Release of version 1.0